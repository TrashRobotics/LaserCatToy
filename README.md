<h1 align="center">
  <a href="https://youtu.be/sZ_J02TyHJI"><img src="https://codeberg.org/TrashRobotics/LaserCatToy/raw/branch/main/img/laser_toy.jpg" alt="Лазерная башня" width="800"></a>
  <br>
    Лазерная башня для котиков
  <br>
</h1>

<p align="center">
  <a href="https://codeberg.org/TrashRobotics/LaserCatToy/src/branch/main/README.md">Русский</a> •
  <a href="https://codeberg.org/TrashRobotics/LaserCatToy/src/branch/main/README-en.md">English(Английский)</a> 
</p>

# Описание проекта
Автоматическая лазерная башня для котиков

# Основные детали
* arduino Nano v3 (micro USB);
* xl4005 понижающий dc/dc преобразователь;
* 2.1 мм jack разъем питания к клеммниками;
* блок питания 12В 1А с выходом 2.1мм jack;
* Лазерная указка или лазерный модуль KY-008;
* 2 x sg90 сервопривод;
* [звенья, корпус и т.д.](https://www.thingiverse.com/thing:5090294)

### Крепеж
* Саморез DIN7981 2.9x9.5 x12;
* Саморез DIN7982 2.2x9.5 x4;

# Схема подключения
![Схема подключения](https://codeberg.org/TrashRobotics/LaserCatToy/raw/branch/main/img/schematic.png)

# Прошивка
Прошивка для arduino Nano: **laser_toy / laser_toy.ino** (русские комментарии)     
**laser_toy / laser_toy_en.ino** (английские комментарии)    
Другие подробности смотрите в видео.

